import 'dart:io';
import 'dart:math';
void main(List<String> arguments) {
  List infix = [];
  List posfix = [];
  List operator = [];
  String template = "";
  List values = [];
  print("Input mathematical expression : ");
  String mathinput = stdin.readLineSync()!;
  token(mathinput ,infix ,template);
  print("Infix : ");
  print(infix);
  checkPosfix(infix ,posfix ,operator);
  print("Posfix : ");
  print(posfix);
  EvaluPosfix(posfix ,values);
  print("Evalution Posfix : ");
  print(values);
}

List token(String mathinput ,List infix ,String template){
  for (var i = 0; i < mathinput.length; i++) {
    if (mathinput[i] == " ") {
      if (template.isNotEmpty) {
        infix.add(template);
        template = "";
      }
    }else if (mathinput[i] == "+" || mathinput[i] == "-" || mathinput[i] == "*" || mathinput[i] == "/" || mathinput[i] == "^" || mathinput[i] == "(" || mathinput[i] == ")"){
      if (template.isNotEmpty) {
        infix.add(template);
        template = "";
      }
      infix.add(mathinput[i]);
    }else {
      template = template + mathinput[i];
    }
  }
  if (!mathinput[mathinput.length - 1].contains(" ")) {
    infix.add(template);
  }
  return infix;
}

List checkPosfix(List infix ,List posfix ,List operator){
  for(var i = 0; i < infix.length; i++){
    if (isNumeric(infix[i])){
      posfix.add(infix[i]);
    }if(infix[i] == "+" || infix[i] == "-" || infix[i] == "*" || infix[i] == "/" || infix[i] == "^"){
      while (operator.isNotEmpty && operator[operator.length-1] != "(" && checkOperator(infix[i]) < checkOperator(operator[operator.length-1])){
        posfix.add(operator[operator.length-1]);
        operator.removeAt(operator.length-1);
      }
      operator.add(infix[i]);
    }if(infix[i] == "("){
      operator.add(infix[i]);
    }if(infix[i] == ")"){
      while(operator[operator.length-1] != "("){
        posfix.add(operator[operator.length-1]);
        operator.removeAt(operator.length-1);
      }
      operator.removeAt(operator.length-1);
    }
  }
  while (operator.isNotEmpty){
    posfix.add(operator[operator.length-1]);
    operator.removeAt(operator.length-1);
  }
  return posfix;
}

List EvaluPosfix(List posfix ,List values){
  int right;
  int left;
  var sum;
  for(var i = 0 ; i < posfix.length; i++){
    if(isNumeric(posfix[i])){
      values.add(posfix[i]);
    }else{
      right = int.parse(values[values.length-1]);
      values.removeLast();
      left = int.parse(values.first);
      values.removeAt(0);
      if(posfix[i] == "+"){
        
        sum = left + right;
      }
      else if(posfix[i] == "-"){
        sum = left - right;
      }
      else if(posfix[i] == "*"){
        sum = left * right;
      }
      if(posfix[i] == "/"){
        sum = left / right;
      }
      else if(posfix[i] == "^"){
        sum = pow(left,right);
      }
      values.add(sum.toString());
    }
  }
  return values;
}

bool isNumeric(String str) {
  for (int i = 0; i < 10; i++){
    if(str[0] == i.toString()){
      return true;
    }
  }
  return false;
}

int checkOperator(String str){
  switch (str){
    case "+":
      return 1;
    case "-":
      return 1;
    case "*":
      return 2;
    case "/":
      return 2;
    case "^":
      return 3;
  }
  return -1;
}
